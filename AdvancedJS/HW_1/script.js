class Employee{
  constructor(name, age, salary){
    this._name=name;
    this._age=age;
    this._salary=salary;
  }

  set name(value){
    this._name=value;
  }
  get name(){
    return this._name
  }
  set age(value){
    this._age=value;
  }
  get age(){
    return this._age
  }
  set salary(value){
    this._salary=value;
  }
  get salary(){
    return this._salary
  }
}

class Programmer extends Employee{
  constructor(name, age, salary, lang='ukr'){
    super(name, age, salary)
    this.lang=lang
  }
  set salary(value){
    this._salary=value;
  }
  get salary(){
    return this._salary * 3
  }
}

const empGena = new Employee('Gena', 39, 1000)
console.log(empGena)
const progTim = new Programmer('Tim', 19, 2000, 'eng')
console.log(progTim)
console.log(progTim.salary)
progTim.salary=3000
console.log(progTim)
console.log(progTim.salary)

console.log(empGena.name)
console.log(progTim.name)

const progAlex = new Programmer('Alex', 26, 1500, 'polish')
console.log(progAlex)
console.log(progAlex.salary)
progAlex.salary=2000
console.log(progAlex)
console.log(progAlex.salary)
console.log(progAlex.lang)
