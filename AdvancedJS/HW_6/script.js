
const getID = 'https://api.ipify.org/?format=json';
const info='http://ip-api.com/json/'
const btnContainer = document.querySelector('.btn-container')
const userResult= document.querySelector('.userResult')
const button= document.querySelector('.btn')

const loader= document.querySelector('.lds-ripple')

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {  //{}
    // mode: 'no-cors',
      method,
      ...config
  }).then(response => {
      if(response.ok) {
        return response.json()
      }
      else {
          return new Error('Чтото пошло не так');
      }
  })
}

const render = async (url) => { // загоняем юзеров
	const responseUser =await sendRequest(url)  // загоняем на получение ID
 
  const userIP = await  responseUser.ip; // находим IP
  console.log(userIP)

  const getInfo = await sendRequest(`${info}${userIP}`) // Инфа на основе ID
  console.log(getInfo)
  
  const {country, city, regionName, timezone, region} = getInfo;
  userResult.innerHTML=`
      <p>Country - ${country}</p>
      <p>City - ${city}</p>
      <p>Region name - ${regionName}</p>
      <p>Timezone - ${timezone}</p>
      <p>Region - ${region}</p>
  `
}

button.addEventListener('click', ()=>{
  loader.style.display='block'
  setTimeout(()=>{
    render(getID)
    loader.style.display='none'
  }, 2000)
})

