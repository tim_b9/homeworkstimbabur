
const API = 'https://ajax.test-danit.com/api/json/';
const APIusers = 'https://ajax.test-danit.com/api/json/users';
const APIposts = 'https://ajax.test-danit.com/api/json/posts'
const cardDeck = document.querySelector('.post-container')
const loader= document.querySelector('.lds-ripple')
const deleteBtn= document.querySelector('.deleteBtn')
const btn = document.querySelector("#btn-new-user");
const BODY_SCROLL = 'modal-open';

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {  //{}
      method,
      ...config
  }).then(response => {
      if(response.ok && method!=='DELETE') {
        return response.json()
      }
      if(method==='DELETE'){
        return response
      }
      else {
          return new Error('Что-то пошло не так');
      }
  })
}
//add request
const addUser = (newUser) => sendRequest(`https://ajax.test-danit.com/api/json/users`, 'POST', { // Добавить нового пользователя
	body: newUser
});
//add request ends

const deletePost = (id) => sendRequest(`https://ajax.test-danit.com/api/json/posts/${id}`, 'DELETE');

class Employee {
  constructor(name, email, title, body,id, postId) {
      this.name = name;
      this.title = title;
      this.body = body;
      this.email = email;
      this.id = id;
      this.postId= postId;
  };

  makePost() {
      const divPost = document.createElement('div');
      divPost.classList.add('card-wrapper')
      divPost.innerHTML =
      `
      <div class="card" id="${this.postId}">
      <p class="card-id pClass" value="${this.id}">Посты юзера:</br>${this.id}</p>
      <p class="card-name pClass">Name</br>${this.name}</p>
      <p class="card-email pClass">Email</br>${this.email}</p>
      <p class="card-title pClass">Title</br>${this.title}</p>
      <p class="card-title pClass">Body</br>${this.body}</p>
      <p class="card-title pClass">PostId</br>${this.postId}</p>
      <button class="deleteBtn btn">Delete</button>    
      </div>
      `
    return cardDeck.append(divPost)
  }
  createPost(){
    const divPost = document.createElement('div');
      divPost.classList.add('card-wrapper')
      divPost.innerHTML =
      `
      <div class="card" id="${this.postId}">
      <p class="card-id pClass" value="${this.id}">Посты юзера:</br>${this.id}</p>
      <p class="card-name pClass">Name</br>${this.name}</p>
      <p class="card-email pClass">Email</br>${this.email}</p>
      <p class="card-title pClass">Title</br>${this.title}</p>
      <p class="card-title pClass">Body</br>${this.body}</p>
      <p class="card-title pClass">PostId</br>${this.postId}</p>
      <button class="deleteBtn btn">Delete</button>     
      </div>
      `    
    return cardDeck.prepend(divPost)
  }
} 

// modal
class Modal {
    constructor({headerTitle, body, closeOutside = false}) {
        this.headerTitle = headerTitle;
        this.body = body;
        this.closeOutside = closeOutside;
    }

    attachListener() { // метод который отвечает за то что был проверить где производится клик и убирает нашу модалку если клик происходит вне модалки и крестик
        document.body.addEventListener("click", (event) => {
            let modal = event.target.classList.contains('modal');
            let close = event.target.classList.contains('close');

            if (modal && this.closeOutside || close) {
                this.close()
            }
        })
    }

    close() { // метод который удаляет темный беграунд подложку модалки, удаляет модалку и возращаят скрол
        this.background.remove()
        document.body.classList.remove(BODY_SCROLL);
        this.modal.remove()
    }

    renderBackground() { // метод который добавляет темную подложку под попапом
        this.background = document.createElement("div")
        this.background.classList.add('modal-backdrop');
        document.body.append(this.background)
    }

    render() {
        this.modal = document.createElement("div");
        this.modal.classList.add("modal");
        this.modal.style.display = "block";
        document.body.classList.add(BODY_SCROLL);
        this.modal.innerHTML = `
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">${this.headerTitle}</h5>
                        <button type="button" class="close">
                            <span aria-hidden="true" style="pointer-events: none;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
<!--                       ${this.body ? this.body : ''} так не работает потому что это умные кавычки и там получаем не стуртуру html а обьект --> 
                    </div>
                </div>
            </div>
        `
        if (this.body) {
             this.modal.querySelector('.modal-body').append(this.body);
        }

        this.renderBackground();
        this.attachListener();
        return this.modal
    }
}
// modal ends
// FORM IN MODAL
class UserForm {
  sendUserData(form,wrapper) { // метод который навешивает слушатель для сабмита, и при сабмите собираем все данные для создания нового пользователя
      form.addEventListener('submit', (event) => {
          event.preventDefault();
          let fullName = event.target.querySelector('[name="fullname"]').value;
          let email = event.target.querySelector('[name="email"]').value;
          let title = event.target.querySelector('[name="title"]').value;
          let body = event.target.querySelector('[name="body"]').value;
          let userId = event.target.querySelector('[name="user-id"]').value;
          let postId = event.target.querySelector('[name="post-id"]').value;
          let background = document.querySelector('.modal-backdrop') // Находим элементы по модалкам что бы его убрать после отправки данных
          let modal = document.querySelector('.modal') // Находим элементы по модалкам что бы его убрать после отправки данных

          if (document.body.classList.contains('modal-open') || background || modal) { // проверям и удаляем все что связано по модалке
              document.body.classList.remove('modal-open')
              background.remove()
              modal.remove()
          }
          if (fullName !== '' && email !== '' && title !== '' && body !== '') {// проверям наши инуты на пустоту и если не пустые то создаем пользователя
              addUser(JSON.stringify({
                  name: fullName,
                  email: email,
                  title: title,
                  body: body,
                  userId:userId,
                  postId:postId,
              }))
                  .then(user => {
                      if(user){ // проверяем отправленые обьект если он есть значит генерим новую карточку пользователя и показываем алерт что все хорошо, а если нет то показываем ошибку алерт
                          const card = new Employee(user.name, user.email, user.title, user.body, user.userId, user.postId);
                          card.createPost(wrapper);
                          
                          let deleteButtons = document.querySelectorAll('.deleteBtn')
                          //  console.log(deleteButtons)
                            deleteButtons.forEach(button=>{
                               button.addEventListener('click', (event)=>{
                                 const delTarget = event.target.closest(".card");
                                 console.log(delTarget)
                                 const targetId=delTarget.id;
                                 deletePost(targetId)
                                 delTarget.remove()
                                 console.log(targetId)
                               })
                            })
                      }
                  })
          }
      })
  }

  render(wrapper) {
      this.formElem = document.createElement('form')
      this.formElem.insertAdjacentHTML("afterbegin", `
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>Full Name</p>
                  <input id="full-name" class="form-control" name="fullname" placeholder="Full Name">
              </label>
          </div>
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>Email</p>
                  <input id="email" class="form-control" name="email" placeholder="Email">
              </label>
          </div>
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>Title</p>
                  <input id="title" class="form-control" name="title" placeholder="Title...">
              </label>
          </div>
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>Body</p>
                  <input id="body" class="form-control" name="body" placeholder="Body...">
              </label>
          </div>
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>User ID</p>
                  <input id="user-id" class="form-control" name="user-id" placeholder="user-id">
              </label>
          </div>
          <div class="form-group">
              <label class="form-label" style="width: 100%;">
                  <p>POST ID</p>
                  <input id="user-id" class="form-control" name="post-id" placeholder="Post-id">
              </label>
          </div>
          
          <button type="submit" class="btn btn-primary">Add</button>
      `)
      this.sendUserData(this.formElem,wrapper)
      return this.formElem;
  }
}
//end form
// on click ADD NEW USER
btn.addEventListener('click', ()=>{
  const newForm = new UserForm();

  const newUserModal = new Modal ({
      headerTitle: 'Add new user',
      body: newForm.render(cardDeck),
      closeOutside: true
  })
  document.body.append(newUserModal.render());
}) 



const render = async (url) => { // загоняем юзеров
	const responseUser =await sendRequest(url)  // загоняем юзеров
  loader.style.display='block'
  // console.log(responseUser)
    if(responseUser){
        loader.style.display='none'
    }

    const responsePost = await sendRequest(APIposts)//загоняем 100 постов
        loader.style.display='block'; //ERROR
        try{
          if(responsePost){
          loader.style.display='none'
        }}
        catch(error){
            console.log(error.message)
        }
      
        responseUser.forEach(({name,email, id}) => { // проходим по каждому юзеру
          const filterPost = responsePost.filter(post=>post.userId===id) // выбираем подходящих
          // console.log(filterPost)
          filterPost.forEach(({title, body, id:postId})=>{ // прогоняем каждый подходящий пост из массива filterPost
             const card = new Employee(name, email, title, body, id, postId).makePost();// делаем карточку     
           })        
         });

         let deleteButtons = document.querySelectorAll('.deleteBtn')
        //  console.log(deleteButtons)
          deleteButtons.forEach(button=>{
             button.addEventListener('click', (event)=>{
               const delTarget = event.target.closest(".card");
               console.log(delTarget)
               const targetId=delTarget.id;
               deletePost(targetId)
               delTarget.remove()
               console.log(targetId)
             })
          })    
}
render(APIusers)
