const API = 'https://ajax.test-danit.com/api/swapi/films'
const filmContainer= document.querySelector('.film-container')
const loader= document.querySelector('.lds-ripple')

// 1

const render = (url) => {
	fetch(url)
		.then(response => response.json())
    .then(data =>{
      console.log(data) //6 films
      data.forEach(({episodeId, name, openingCrawl, characters}) => { 

        fetch("https://ajax.test-danit.com/api/swapi/people/") // все персонажи
        .then(response=>response.json())
        .then(people=>{
        const filterArray = people.filter(human=>characters.includes(human.url)) //сортируем из всех, тех кто содержится в characters. Если human.url есть в characters у этого фильма, то идёт в filterArray
         document.body.insertAdjacentHTML('afterend', ` 
            <div class="film">
              <p class="film-id">Film id - ${episodeId}</p>
              <p class="film-name">Name - ${name}</p>
              <p class="film-openingCrawl">Crawl - ${openingCrawl}</p>
              <h2>Characters:</h2>
              <ul class="characters"></ul>
            </div>`)
        
         const ul=document.querySelector('.characters');
          filterArray.forEach(({name})=>{
            ul.insertAdjacentHTML('beforeend', `
            <li>${name}</li>
            `)
          })
          console.log(filterArray)
        })
        .catch(error=>console.log(error.message))
      
      });
      
    })
    .catch(error=>console.log(error.message))
    .finally(loader.remove())
}
render(API)


/*
// УНИВЕРСАЛЬНАЯ 
const sendRequest= async(url)=>{
  const response = await fetch(url);
  const result = await response.json();
  return result
}


//2 БОЛЕЕ УНИВЕРСАЛЬНАЯ
const render=(url) =>{
  sendRequest(url) //получаем результат
  .then(films =>{
    console.log(films) //6 films
    films.forEach(({episodeId, name, openingCrawl, characters}) => { 

      fetch("https://ajax.test-danit.com/api/swapi/people/") // все персонажи
      .then(response=>response.json())
      .then(people=>{
      const filterArray = people.filter(human=>characters.includes(human.url)) //сортируем из всех, тех кто содержится в characters. Если human.url есть в characters у этого фильма, то идёт в filterArray
       document.body.insertAdjacentHTML('afterend', ` 
          <div class="film">
            <p class="film-id">Film id - ${episodeId}</p>
            <p class="film-name">Name - ${name}</p>
            <p class="film-openingCrawl">Crawl - ${openingCrawl}</p>
            <h2>Characters:</h2>
            <ul class="characters"></ul>
          </div>`)
      
       const ul=document.querySelector('.characters');
        filterArray.forEach(({name})=>{
          ul.insertAdjacentHTML('beforeend', `
          <li>${name}</li>
          `)
        })
        console.log(filterArray)
      })
      .catch(error=>console.log(error.message))
    
    });
  })
  .catch(error=>console.log(error.message))
  .finally(loader.remove())
}
render(API)

*/