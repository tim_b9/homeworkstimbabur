import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {sendRequest} from '../helpers';

export const actionFetchProducts = createAsyncThunk(
    'products/fetchData',
    async () => {
    const response = await sendRequest(`${window.location.origin}/goodsJson.json`)
    return response
})

const initialState = {
    data: [],
    shoppingCard: [],
    favouriteCard: []
}

const productsSlice = createSlice({
    name: "apiData",
    initialState,
    reducers: {
        // actionData : (state, {payload}) => {
        //     state.data = payload;
        // }, 
        actionShoppingCard : (state, {payload}) => {
            state.shoppingCard = payload
        },
        actionRemoveShoppingCard : (state, {payload}) => {
            state.shoppingCard = state.shoppingCard.filter(item => item.orderNumber !== payload)
        },
        actionFavouriteCard : (state, {payload}) => {
            state.favouriteCard = payload
        },
        actionRemoveFavouriteCard : (state, {payload}) => {
            state.favouriteCard = state.favouriteCard.filter(item => item.orderNumber !== payload)
        },
    }, 
    extraReducers: (builder)=>{
        builder.addCase(actionFetchProducts.fulfilled, (state, {payload}) => {
            state.data = payload;
        })
    }
})

export const {actionShoppingCard,actionRemoveShoppingCard, actionFavouriteCard, actionRemoveFavouriteCard} = productsSlice.actions

export default productsSlice.reducer 
