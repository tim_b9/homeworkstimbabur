import modalReducer, {actionOpen1, actionClose1, actionOpen2, actionClose2} from "./modal.reducer";
import productsReducer, {actionFetchProducts,actionShoppingCard,actionRemoveShoppingCard, actionFavouriteCard, actionRemoveFavouriteCard} from "./products.reducer";

export {
	modalReducer,
	productsReducer,
	actionOpen1,
	actionClose1,
	actionOpen2,
	actionClose2,
	actionFetchProducts,
	actionShoppingCard,
	actionRemoveShoppingCard,
	actionFavouriteCard,
	actionRemoveFavouriteCard
}

/*action Функции для того что бы вызвать функцию reducer которая должна изменить значение в state при ииспользовании хука useDispatch*/