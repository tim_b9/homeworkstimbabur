import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    isOpenFirst: false,
    isOpenSecond: false,
}

export const modalSlice = createSlice({
    name: "modal",
    initialState,
    reducers: {
        actionOpen1: (state) => {
            state.isOpenFirst = true
        }, 
        actionClose1: (state) => {
            state.isOpenFirst = false
        },
        actionOpen2: (state) => {
            state.isOpenSecond = true
        }, 
        actionClose2: (state) => {
            state.isOpenSecond = false
        }
    }
})

export const {actionOpen1, actionClose1, actionOpen2, actionClose2} = modalSlice.actions;
export default modalSlice.reducer;
