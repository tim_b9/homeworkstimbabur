import {selectModalFirst, selectModalSecond} from "./modal.selector";
import { selectProductsData,selectShoppingCard, selectStateProducts, selectState, selectFavouriteCard } from "./products.selector";

export { selectModalFirst, selectModalSecond,selectShoppingCard, selectProductsData, selectStateProducts, selectState, selectFavouriteCard}

/*select Функции для того что бы получать данные из нашего стора при ииспользовании хука useSelector*/