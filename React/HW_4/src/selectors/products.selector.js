export const selectProductsData = (state) => state.products.data;
export const selectShoppingCard = (state) => state.products.shoppingCard;
export const selectFavouriteCard = (state) => state.products.favouriteCard;
export const selectStateProducts = (state) => state.products;
export const selectState = (state) => state;