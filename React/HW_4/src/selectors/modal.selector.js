export const selectModalFirst = (state) => state.modal.isOpenFirst;
export const selectModalSecond = (state) => state.modal.isOpenSecond;