import {configureStore} from '@reduxjs/toolkit';

// import thunk from 'redux-thunk'
// import logger from 'redux-logger';

import {modalReducer, productsReducer} from '../reducers'

const store = configureStore({
    reducer: {
        modal: modalReducer, 
        products: productsReducer,
    },
    // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger,thunk)
})

export default store 