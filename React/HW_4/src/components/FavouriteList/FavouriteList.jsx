import './FavouriteList.scss'
import { Link } from 'react-router-dom';
import {useSelector} from "react-redux";
import {selectFavouriteCard} from "../../selectors";

function FavouriteList(){
    const favouriteCard = useSelector(selectFavouriteCard)
    return (
        <div className="wrapper">
            <div className='FavouriteList'>
                {'Итого в избранных - '} {favouriteCard.length}
            </div>
            <Link to="/FavouriteListPage" className="favouritePage">Отобразить избранное</Link>
        </div>   
    )
}
export default FavouriteList;