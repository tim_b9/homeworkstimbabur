import {useState, useEffect, useMemo} from 'react';
import Cards from './components/Cards';
import {useDispatch, useSelector} from 'react-redux';
// getProducts
import {selectProductsData} from '../../selectors';
// async request
import {actionFavouriteCard, actionFetchProducts, actionShoppingCard} from '../../reducers';

const CardsList = () => {    
    const defaultArray = localStorage.getItem("buyBin") ? JSON.parse(localStorage.getItem("buyBin")) : []
    const defaultArrayFavoutite = localStorage.getItem("favourite") ? JSON.parse(localStorage.getItem("favourite")) : []

    const [favourite, setFavourite] = useState(defaultArrayFavoutite);
    const [buyBin, setBuyBin] = useState(defaultArray);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(actionFetchProducts())
    }, [dispatch])

    useMemo(() => dispatch(actionShoppingCard(buyBin)), [buyBin]); //записываем значение в стор что бы получать в хеадере каунтер
    useMemo(() => dispatch(actionFavouriteCard(favourite)), [favourite]); //записываем значение в стор что бы получать в хеадере каунтер

    const productsState = useSelector(selectProductsData)
     
    return (
        <> 
            <Cards goodsList={productsState} favourite={favourite}
                   setFavourite={setFavourite}
                   buyBin={buyBin}
                   setBuyBin={setBuyBin}/>
        </>
    )
}

export default CardsList
