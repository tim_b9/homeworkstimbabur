import PropTypes from 'prop-types';
import Button from '../../../Modal/components/Button';
import {useDispatch} from 'react-redux';
import {actionOpen1, actionOpen2} from '../../../../reducers';

const Card = ({
				  cardProps,
				  isFavourite,
				  isBuyBin,
	              setProId,
			}) => {
	const {productName, price, orderNumber, picture, color,} = cardProps;

	const dispatch = useDispatch();

	return (
		<div className='product' id={orderNumber}>

			<div className={isFavourite ? 'star active' : 'star'} onClick={
				() => {
					dispatch(actionOpen2())
					setProId(orderNumber)
				}
			}>
			</div>

			<p className="productName">Product Name - {productName}</p>
			<p className="price">Price - {price}</p>
			<p className="orderNumber">Order Number - {orderNumber}</p>
			<p className="color">Color - {color}</p>
			<img alt={productName} src={picture} className="picture"/>
			<div>
				{isBuyBin && <Button className='button-remove' textBtn='Убрать из корзины!' onClick={() => {
					dispatch(actionOpen1())
					setProId(orderNumber)
				}}/>}
				{!isBuyBin && <Button className='button' textBtn='Добавить в корзину!' onClick={() => {
					dispatch(actionOpen1())
					setProId(orderNumber)
				}}/>}
			</div>
		</div>
	)
}

Card.propTypes = {
	cardProps: PropTypes.object.isRequired,
	onClick: PropTypes.func
}
Card.defaultProps = {
	productName: "Product does not have name",
	color: "Product does not have color"
}

export default Card;