import PropTypes from 'prop-types';
import Card from './Card';
import './Cards.scss'
import Modal from "../../../Modal";
import {actionClose1, actionClose2,actionShoppingCard, actionFavouriteCard} from "../../../../reducers";
import {useDispatch, useSelector} from "react-redux";
import {selectModalFirst, selectModalSecond} from "../../../../selectors";
import {useState} from "react";

const Cards =({goodsList,favourite, setFavourite, buyBin, setBuyBin}) => {

    const dispatch = useDispatch();
    const modal1 = useSelector(selectModalFirst)
    const modal2 = useSelector(selectModalSecond)
    const [id, setId] = useState(0);

    const [first] = useState({
        headerText1: 'Вы хотите добавить машину?',
        modalText1: 'Для продолжения нажмите - Да'
    });
    const [second] = useState({
        headerText2: 'Добавить машину в избранное?',
        modalText2: 'Для продолжения нажмите - Да'
    });

	const approveAdding = (id) => {

		setBuyBin((prev) => {
			if (prev.includes(id)) {
				const filterProduct = prev.filter(number => {
					console.log(number)
					return number !== id
				})
				localStorage.setItem('buyBin', JSON.stringify(filterProduct))
				localStorage.setItem('buyBinQuantity', filterProduct.length)
				console.log(filterProduct)
				return filterProduct
			} else {
				const changeArrBin = [...prev, id]
				localStorage.setItem('buyBin', JSON.stringify(changeArrBin))
				localStorage.setItem('buyBinQuantity', changeArrBin.length)
				return changeArrBin
			}

		})
		// wishList.innerText = 'Итого в корзине - ' + buyBinQuantity
	}

	const favouriteAdding = (id) => {
		setFavourite(prev => {
			if (prev.includes(id)) {
				const filterProduct = prev.filter(number => {
					console.log(number)
					return number !== id
				})
				localStorage.setItem('favourite', JSON.stringify(filterProduct))
				localStorage.setItem('favouriteQuantity', filterProduct.length)
				console.log(filterProduct)
				return filterProduct
			} else {
				const changeArr = [...prev, id]
				localStorage.setItem('favourite', JSON.stringify(changeArr))
				localStorage.setItem('favouriteQuantity', changeArr.length)
				return changeArr
			}
		})
		// FavouriteList.innerText = 'Итого в избранных - ' + favouriteListQuantity
		// console.log('change')
	}

    const goods = goodsList.map((card) => <Card
            key={card.orderNumber}
            cardProps={card}
            setProId={setId}
		    isBuyBin={buyBin.includes(card.orderNumber)}
		    isFavourite={favourite.includes(card.orderNumber)}
        />)

    return (
        <>
            <div className="products">
                {goods}
            </div>

            {modal1 &&
                <Modal
                    headerText={first.headerText1}
                    modalText={`${first.modalText1}`}
                    click={() => dispatch(actionClose1())}
                    approve={() => {
                        dispatch(actionClose1())
                        dispatch(actionShoppingCard(id))
	                    approveAdding(id)
                    }}
                />
            }
            {modal2 &&
                <Modal
                    headerText={second.headerText2}
                    modalText={`${second.modalText2}`}
                    click={() => dispatch(actionClose2())}
                    approve={() => {
                        dispatch(actionClose2());
                        dispatch(actionFavouriteCard(id));
	                    favouriteAdding(id);
                    }}
                />
            }
        </>
    )
}
Cards.propTypes = {
    goodsList: PropTypes.array.isRequired
}

export default Cards;