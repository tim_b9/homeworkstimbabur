import {useState, useEffect} from 'react';
import {sendRequest} from '../../helpers';
import Cards from '../CardsList/components/Cards';

function WishListPage(){
    const defaultArray = localStorage.getItem("buyBin") ? JSON.parse(localStorage.getItem("buyBin")) : []
    const defaultArrayFavoutite = localStorage.getItem("favourite") ? JSON.parse(localStorage.getItem("favourite")) : []
    const [goodsArr, setGoods] = useState([]);
    const [buyBin, setBuyBin] = useState(defaultArray);
    const [favourite, setFavourite] = useState(defaultArrayFavoutite);
    useEffect(() => {
        sendRequest('./goodsJson.json')
            .then((response) => {
                setGoods(response)  
            })
    }, []) 
    // console.log(buyBin)
    const filter = goodsArr.filter((item)=>buyBin.includes(item.orderNumber)) //проверяю какие карточки отображать (нужно те, которые в массиве buyBin лежат)
    // console.log('filter', filter)
    let buyBinQuantity = localStorage.getItem('buyBinQuantity') //отображение кол-ва
    let favouriteListQuantity = localStorage.getItem('favouriteQuantity') //отображение кол-ва

    return (
       <>
       <div className="wrapper">
            <div className='wishList'>
                {'Итого в корзине - '} {buyBinQuantity}
            </div>
       </div>
       <div className="wrapper">
            <div className="FavouriteList">  
                {'Итого в избранных - '} {favouriteListQuantity}
            </div>
       </div>
          <Cards goodsList={filter} buyBin={buyBin} setFavourite={setFavourite} favourite={favourite} setBuyBin={setBuyBin} />
          {/* формирую карточки, которые прилетели в filter */}
       </> 
    )
}
export default WishListPage;