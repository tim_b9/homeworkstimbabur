import {useState, useEffect} from 'react';
import {sendRequest} from '../../helpers';
import Cards from '../CardsList/components/Cards';

function FavouriteListPage(){ 
    const defaultArrayFavoutite = localStorage.getItem("favourite") ? JSON.parse(localStorage.getItem("favourite")) : [];
    const defaultArray = localStorage.getItem("buyBin") ? JSON.parse(localStorage.getItem("buyBin")) : []  
    const [goodsArr, setGoods] = useState([]);
    const [favourite, setFavourite] = useState(defaultArrayFavoutite);
    const [buyBin, setBuyBin] = useState(defaultArray);
    useEffect(() => {
        sendRequest('./goodsJson.json')
            .then((response) => {
                setGoods(response)
            })

    }, []) 
    // console.log(favourite)
    const filter = goodsArr.filter((item)=>favourite.includes(item.orderNumber)) //проверяю какие карточки отображать (нужно те, которые в массиве favourite лежат)
    // console.log('filter', filter)
    let buyBinQuantity = localStorage.getItem('buyBinQuantity') //отображение кол-ва
    let favouriteListQuantity = localStorage.getItem('favouriteQuantity') //отображение кол-ва

    return (
       <>
       <div className="wrapper">
            <div className='wishList'>
                {'Итого в корзине - '} {buyBinQuantity}
            </div>
       </div>
       <div className="wrapper">
            <div className="FavouriteList">  
                {'Итого в избранных - '} {favouriteListQuantity}
            </div>
       </div>
          <Cards goodsList={filter} setBuyBin={setBuyBin} buyBin={buyBin} favourite={favourite} setFavourite={setFavourite} /> 
           {/* формирую карточки, которые прилетели в filter */}
       </> 
    )
}
export default FavouriteListPage;