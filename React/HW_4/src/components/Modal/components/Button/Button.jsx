import PropTypes from 'prop-types';
import './Button.scss'

function Button({textBtn, onClick, type, className}){
    return (
        <button className={className}  type={type} onClick={onClick}>{textBtn}</button>
    )
}

Button.defaultProps = {
    type: 'button',
    textBtn: "Default button text"
}
Button.propTypes = {
    onClick: PropTypes.func,
}

export default Button;