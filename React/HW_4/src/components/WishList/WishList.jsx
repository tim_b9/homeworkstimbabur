import './WishList.scss'
import { Link } from 'react-router-dom';
import {useSelector} from "react-redux";
import { selectShoppingCard} from "../../selectors";
function WishList(){
    const shoppingCard = useSelector(selectShoppingCard)

    return (
        <div className="wrapper">
            <div className='wishList'>
                    {'Итого в корзине - '} {shoppingCard.length}
            </div>
            <Link to="/WishListPage" className="favouritePage">Отобразить корзину</Link>
        </div>   
    )
}
export default WishList;