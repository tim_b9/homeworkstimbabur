import React, {Component} from 'react';

import './Button.scss'

class Button extends Component{
    render() {
        const {textBtn, onClick, type} = this.props;        
        return (
            <button className="button" type={type} onClick={onClick}>{textBtn}</button>
        )
    }
}

Button.defaultProps = {
    type: 'button'
}

export default Button;