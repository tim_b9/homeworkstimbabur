import React, {Component} from 'react';

import './ModalText.scss'

class ModalText extends Component{
    render() {
        const {modalText} = this.props;
        return (
            <div className="modalText">{modalText}</div>
        )
    }
}



export default ModalText;