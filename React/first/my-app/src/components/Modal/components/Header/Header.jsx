import React, {Component} from 'react';
import Button from '../Button';
import './Header.scss'

class Header extends Component{
    render() {
        const {headerText, closeFunc} = this.props;
        // console.log('Button', this.props);
        
        return (
            <>
            <div className="header">{headerText}<Button textBtn='X' onClick={closeFunc}/></div>
            <div> </div>
            </>
        )
    }
}



export default Header;