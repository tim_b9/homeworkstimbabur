import React, {Component} from 'react';
import Button from '../Button';

class Controllers extends Component {

    render() {
        const {okayPress, cancelPress} = this.props;
        return (
            <div className="widget-controllers">
                <div className="button-container">
                    <Button textBtn='OK' onClick={okayPress}/>
                    <Button textBtn='Cancel' onClick={cancelPress}/>
                </div>
            </div>
        )
    }
}

export default Controllers;