import React, {Component} from 'react';
import './Modal.scss'
// import Button from "./components/Button";
import Controllers from "./components/Controllers";
import Header from "./components/Header";
import ModalText from "./components/ModalText";

class Modal extends Component{
    
    render(){
        
        const {headerText, modalText, click} = this.props;
        return(
            <div className="modal">
            <Header headerText={headerText}  closeFunc={click}/>
            <ModalText modalText={modalText}/>
            <Controllers okayPress={click} cancelPress={click} />
            </div> 
        
        )
    }
}

export default Modal