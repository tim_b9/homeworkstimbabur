import React, {Component} from 'react';
import Button from './components/Modal/components/Button';
import Modal from './components/Modal';



class App extends Component {
    state = {
      modal1:false,
      modal2:false,

      first:{
        headerText1: 'First header 1',
        modalText1: 'Modal Text 1'
      },
      second:{
        headerText2: 'Second header 2',
        modalText2: 'Modal Text 2'
      },
     
    };

    // increase = () => {
        
    //     const {temperature} = this.state;
    //     if(temperature >= 29) return;
    //     this.setState({temperature: temperature + 1})
    // }

    // decrease = () => {
    //     const {temperature} = this.state;
    //     if(temperature <= 0) return;
    //     this.setState({temperature: temperature - 1})
    // }

    // displayBg = () => {
    //     const {temperature} = this.state;
    //     if(temperature <= 10) return 'widget-container cold'
    //     if(temperature <= 20) return 'widget-container neutral'
    //     return 'widget-container hot'
    // }
    closeModal1 =() => {
      return this.setState({modal1: false})
    }
    closeModal2 =() => {
      return this.setState({modal2: false})
    }
    render() {
      const {headerText1, modalText1} = this.state.first
      const {headerText2, modalText2} = this.state.second
      
        return (
            <>
              <Button textBtn='Open first modal' onClick={() => this.setState({modal1: true, modal2:false})}/>
              <Button textBtn='Open second modal' onClick={() => this.setState({modal2: true, modal1: false})}/>
              {this.state.modal1 && 
              <div className="modal-wrapper"
                                         onClick={(e) => {
                                             if (e.target === e.currentTarget) {
                                                 this.setState({modal1: false})
                                             }
                                         }
              }>
                <Modal headerText={headerText1} modalText={modalText1} 
                                         click={this.closeModal1}/>
              </div>}

              {this.state.modal2 &&  
              <div className="modal-wrapper"
                                         onClick={(e) => {
                                             if (e.target === e.currentTarget) {
                                                 this.setState({modal2: false})
                                             }
                                         }
              }> 
              <Modal headerText={headerText2} modalText={modalText2} click={this.closeModal2}/>
             </div>}
            </>
        )
    }
}

export default App;
