import CardsList from '../CardsList';
import WishList from '../WishList';
import FavouriteList from '../FavouriteList';

function HomePage(){

    return (
       <>
            <WishList/>
            <FavouriteList/>
            <CardsList/>
       </> 
    )
}

export default HomePage;