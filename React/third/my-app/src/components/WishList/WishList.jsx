import './WishList.scss'
import { Link } from 'react-router-dom';
function WishList(){
    let buyBinQuantity = localStorage.getItem('buyBinQuantity')
    return (
        <div className="wrapper">
            <div className='wishList'>
                    {'Итого в корзине - '} {buyBinQuantity}
            </div>
            <Link to="/WishListPage" className="favouritePage">Отобразить корзину</Link>
        </div>   
    )
}
export default WishList;