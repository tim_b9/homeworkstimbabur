import './Modal.scss'
import Controllers from "./components/Controllers";
import Header from "./components/Header";
import ModalText from "./components/ModalText";

function Modal({headerText, modalText, click, approve}){
    return(
        <>
        <div className="modal-wrapper"
                                 onClick={(e) => {
                                     if (e.target === e.currentTarget) {
                                        click()
                                     }
                                 }
      }>
        <div className="modal">
                <Header headerText={headerText}  closeFunc={click}/>
                <ModalText modalText={modalText}/>
                <Controllers okayPress={approve} cancelPress={click} />
            </div>
        </div>   
        </>
    )
}


export default Modal