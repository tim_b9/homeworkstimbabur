import './ModalText.scss';

function ModalText({modalText}){
    return (
        <div className="modalText">{modalText}</div>
    )
}
export default ModalText;