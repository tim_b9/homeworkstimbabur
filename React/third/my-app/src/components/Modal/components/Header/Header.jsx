import Button from '../Button';
import './Header.scss'
import PropTypes from 'prop-types';

function Header({headerText, closeFunc}){
    return (
        <>
        <div className="header">{headerText}<Button className='button' textBtn='X' onClick={closeFunc}/></div>
        </>
    )
}

Header.propTypes = {
    headerText: PropTypes.string,
    closeFunc: PropTypes.func.isRequired,

}
Header.defaultProps = {
    headerText: "Default header text",

}

export default Header;