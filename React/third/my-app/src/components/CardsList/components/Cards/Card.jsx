import PropTypes from 'prop-types';
import Button from '../../../Modal/components/Button';
import Modal from '../../../Modal';
import {useState} from 'react';

const Card = ({favourite, setFavourite, cardProps, buyBin, setBuyBin, isFavourite, isBuyBin}) => {
    const {productName, price, orderNumber, picture, color, } = cardProps;
    const [modal1, setModal1] = useState(false);
    const [modal2, setModal2] = useState(false);
    const [first] = useState({
    headerText1: 'Вы хотите добавить машину?',
    modalText1: 'Для продолжения нажмите - Да'
    });
    const [second] = useState({
        headerText2: 'Добавить машину в избранное?',
        modalText2: 'Для продолжения нажмите - Да'
    });

    let buyBinQuantity = localStorage.getItem('buyBinQuantity') 
    let wishList = document.querySelector(".wishList")
    wishList.innerText = 'Итого в корзине - ' + buyBinQuantity;

    let favouriteListQuantity = localStorage.getItem('favouriteQuantity')
    let FavouriteList = document.querySelector(".FavouriteList")
    FavouriteList.innerText = 'Итого в избранных - ' + favouriteListQuantity 

const approveAdding = () =>{
    console.log(orderNumber)
    setBuyBin((prev)=>{
        // console.log(orderNumber)
        if(prev.includes(orderNumber)){
           const filterProduct = prev.filter(number=>{
               console.log(number)
               return number!==orderNumber
           }) 
           localStorage.setItem('buyBin', JSON.stringify(filterProduct))
           localStorage.setItem('buyBinQuantity', filterProduct.length)
           console.log(filterProduct)
           return filterProduct
        }

        else{
            const changeArrBin =[...prev, orderNumber]
            localStorage.setItem('buyBin', JSON.stringify(changeArrBin))
            localStorage.setItem('buyBinQuantity', changeArrBin.length)
            return changeArrBin
        }
        
    })   
        wishList.innerText = 'Итого в корзине - ' + buyBinQuantity
        // console.log('change')
}

const favouriteAdding = () =>{
    
    setFavourite(prev=>{
        if(prev.includes(orderNumber)){
            const filterProduct = prev.filter(number=>{
                console.log(number)
                return number!==orderNumber
            }) 
            localStorage.setItem('favourite', JSON.stringify(filterProduct))
            localStorage.setItem('favouriteQuantity', filterProduct.length)
            console.log(filterProduct)
            return filterProduct
        } 

        else{
        const changeArr =[...prev, orderNumber]
        localStorage.setItem('favourite', JSON.stringify(changeArr))
        localStorage.setItem('favouriteQuantity', changeArr.length)
        return changeArr
        }
    })
        FavouriteList.innerText = 'Итого в избранных - ' + favouriteListQuantity 
        // console.log('change')
}
    return (
        <div className='product' id={orderNumber}>
        
        <div className={isFavourite ? 'star active' : 'star'} onClick={
            () => {
                setModal2(true);
                }

            }></div>
        {modal2 && 
                <Modal headerText={second.headerText2} modalText={second.modalText2} 
                                            click={()=>setModal2(false)} approve={
                                                ()=>{favouriteAdding();
                                                setModal2(false);
                                                // setIsFavourite(true)
                                                }}
                                            />
        } 
            <p className="productName">Product Name - {productName}</p>
            <p className="price">Price - {price}</p>
            <p className="orderNumber">Order Number - {orderNumber}</p>
            <p className="color">Color - {color}</p>
            <img alt={productName}  src={picture} className="picture" />
            <div>
            {isBuyBin && <Button className='button-remove' textBtn='Убрать из корзины!' onClick={() => {setModal1(true)}}/> }
            {isBuyBin===false && <Button className='button' textBtn='Добавить в корзину!' onClick={() => {setModal1(true)}}/> }
               
     
                {modal1 && 
                <Modal headerText={first.headerText1} modalText={first.modalText1} 
                                            click={()=>setModal1(false)} approve={()=>{approveAdding();setModal1(false)}}
                                            />
                } 
            </div> 
                   
        </div>
    )
}

Card.propTypes = {
    cardProps: PropTypes.object.isRequired,
    onClick: PropTypes.func
}
Card.defaultProps = {
    productName: "Product does not have name",
    color: "Product does not have color"
}

export default Card;