import PropTypes from 'prop-types';
import Card from './Card';
import './Cards.scss'

const Cards =({goodsList,favourite,setFavourite, buyBin, setBuyBin})=>{ /// props => {} атрибутов компонента(название атрибута это ключ в обьекте)
    
    const goods = goodsList.map((card) => <Card favourite={favourite} setFavourite={setFavourite} buyBin={buyBin} setBuyBin={setBuyBin} isBuyBin={buyBin.includes(card.orderNumber)} key={card.orderNumber} cardProps={card} isFavourite={favourite.includes(card.orderNumber)}/>)
    
    
    return (
        <div className="products">
            {goods}
        </div>
    )
}

Cards.propTypes = {
    goodsList: PropTypes.array.isRequired
}

export default Cards;