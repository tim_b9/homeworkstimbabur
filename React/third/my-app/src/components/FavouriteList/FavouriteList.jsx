import './FavouriteList.scss'
import { Link } from 'react-router-dom';

function FavouriteList(){
    let favouriteListQuantity = localStorage.getItem('favouriteQuantity')
    // if(favouriteListQuantity===null){
    //     favouriteListQuantity=0
    // }
    return (
        <div className="wrapper">
            <div className='FavouriteList'>
                {'Итого в избранных - '} {favouriteListQuantity}
            </div>
            <Link to="/FavouriteListPage" className="favouritePage">Отобразить избранное</Link>
        </div>   
    )
}
export default FavouriteList;