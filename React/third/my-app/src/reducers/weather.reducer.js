import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import {sendRequest} from '../helpers';


// export const actionFetchWeathers = createAsyncThunk('weather/fetch_data', async (city) => {
//     const response = sendRequest(`${API_URL}?key=${API_KEY}&q=${city}&days=5&aqi=no&alerts=no`)
//     return response
// })

const initialState = {
    city:'kiev',
    weatherCity: {},
    loader: true,
}

const weatherSlice = createSlice({
    name: "weather",
    initialState,
    reducers: {
        actionCity: (state,{payload}) => {
            state.city = payload;
        }, // actions, reducer
    }, 
    // extraReducers: (builder)=>{
    //     builder.addCase(actionFetchWeathers.pending, (state) => {
    //         state.loader = true;
    //     })

    //     builder.addCase(actionFetchWeathers.fulfilled, (state, {payload}) => {
    //         state.weatherCity = payload;
    //         state.loader = false;
    //     })
    // }

})

export const {actionCity} = weatherSlice.actions

export default weatherSlice.reducer 
