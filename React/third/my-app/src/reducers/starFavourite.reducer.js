import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    star: false,
}

export const favouriteSlice = createSlice({
    name: "favourite",
    initialState,
    reducers: {
        enableStar: (state) => {
            state.star = true
        }, 
        disableStar: (state) => {
            state.star = false
        }
    }
})

export const {enableStar, disableStar} = favouriteSlice.actions;
export default favouriteSlice.reducer;
