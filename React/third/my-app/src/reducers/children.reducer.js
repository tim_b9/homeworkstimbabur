import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    temperature: 23,
}

export const childrenSlice = createSlice({
    name: "children",
    initialState,
    reducers: {
        actionIncreaseBaby: (state) => {
            state.temperature +=1
        }, // actions, reducer
        actionDecreaseBaby: (state) => {
            state.temperature -=1
        }
    }
})

export const {actionIncreaseBaby, actionDecreaseBaby} = childrenSlice.actions;

export default childrenSlice.reducer;
