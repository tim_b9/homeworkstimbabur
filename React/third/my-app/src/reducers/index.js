import favouriteReducer, {enableStar, disableStar} from "./starFavourite.reducer";
// import childrenReducer, {actionIncreaseBaby, actionDecreaseBaby} from "./children.reducer";
// import weatherReducer, {actionCity, actionFetchWeathers} from './weather.reducer';

export {
//  actionIncreaseBed,
//  actionDecreaseBed,
//  childrenReducer,
//  actionIncreaseBaby,
//  actionDecreaseBaby,
favouriteReducer,
enableStar,
disableStar,
//  actionFetchWeathers,
}

/*action Функции для того что бы вызвать функцию reducer которая должна изменить значение в state при ииспользовании хука useDispatch*/