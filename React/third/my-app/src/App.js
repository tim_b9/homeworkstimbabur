import HomePage from './components/HomePage/HomePage.jsx';
import WishListPage from './components/WishListPage/WishListPage.jsx';
import FavouriteListPage from './components/FavouriteListPage/FavouriteListPage.jsx';

import { Routes, Route} from 'react-router-dom'


function App(){
  return (
    <>
    
        <Routes> 
          <Route path='/' element={<HomePage/>}/>
          <Route path='/WishListPage' element={<WishListPage/>}/>
          <Route path='/FavouriteListPage' element={<FavouriteListPage/>}/>
          
        </Routes>
     
    </>
)
}

export default App;
