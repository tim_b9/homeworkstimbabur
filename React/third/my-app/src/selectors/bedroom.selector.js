export const selectBedroomTemperature = (state) => state.bedroom.temperature;
export const selectBedroom = (state) => state.bedroom;
export const selectState = (state) => state;