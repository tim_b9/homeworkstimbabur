export const selectChildrenTemperature = (state) => state.children.temperature;
export const selectChildren = (state) => state.children;