import {configureStore} from '@reduxjs/toolkit';

// import thunk from 'redux-thunk'
// import logger from 'redux-logger';

import {favouriteReducer} from '../reducers'

const store = configureStore({
    reducer: {
        // bedroom: bedroomReducer, //bedroom.reducer.js
        // children: childrenReducer, //children.reducer.js
        favourite: favouriteReducer, // weather.reducer.js
    },
    // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger,thunk)
})

export default store 