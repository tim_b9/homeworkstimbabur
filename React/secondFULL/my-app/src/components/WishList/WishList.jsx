// import React, {Component} from 'react';
import './WishList.scss'

function WishList(){
    let buyBinQuantity = localStorage.getItem('buyBinQuantity')
    return (
        <div className='wishList'>
        {'Итого в корзине - '} {buyBinQuantity}
        </div>
    )
}

WishList.defaultProps = {
    type: 'button'
}

export default WishList;