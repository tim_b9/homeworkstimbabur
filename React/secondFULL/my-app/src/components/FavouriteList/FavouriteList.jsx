// import React, {Component} from 'react';
import './FavouriteList.scss'

function FavouriteList(){
    
    let favouriteListQuantity = localStorage.getItem('favouriteQuantity')
    
    return (
        <div className='FavouriteList'>
        {'Итого в избранных - '} {favouriteListQuantity}
        </div>
    )
}


export default FavouriteList;