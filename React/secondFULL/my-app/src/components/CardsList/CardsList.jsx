import {useState, useEffect} from 'react';
import Cards from './components/Cards';
import {sendRequest} from '../../helpers';

const CardsList = () => {    
    const defaultArray = localStorage.getItem("buyBin") ? JSON.parse(localStorage.getItem("buyBin")) : []
    const defaultArrayFavoutite = localStorage.getItem("favourite") ? JSON.parse(localStorage.getItem("favourite")) : []
    const [goods, setGoods] = useState([]);
    const [favourite, setFavourite] = useState(defaultArrayFavoutite);
    const [buyBin, setBuyBin] = useState(defaultArray);

    useEffect(() => {
        sendRequest('./goodsJson.json')
            .then((response) => {
                setGoods(response)
                console.log(response)
            })

    }, [])         
           
     
    return (
        <> 
            <Cards goodsList={goods} favourite={favourite} setFavourite={setFavourite} buyBin={buyBin} setBuyBin={setBuyBin}/>
        </>
    )

}


export default CardsList
