// import React, {Component} from 'react';
import Button from '../Button';

function Controllers({okayPress, cancelPress}){
    return (
        <div className="widget-controllers">
            <div className="button-container">
                <Button className='button' textBtn='Да' onClick={okayPress}/>
                <Button className='button' textBtn='Отмена' onClick={cancelPress}/>
            </div>
        </div>
    )
}

export default Controllers;