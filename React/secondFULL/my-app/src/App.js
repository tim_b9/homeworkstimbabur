import CardsList from './components/CardsList';
import WishList from './components/WishList';
import FavouriteList from './components/FavouriteList';

function App(){
  return (
    <>
     <WishList/>
     <FavouriteList/>
     <CardsList/>
    </>
)
}

export default App;
