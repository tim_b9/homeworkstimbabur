// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.

 ///WITHOUT///////
 const createNewUser=() => {
    let firstName= prompt("Enter your name - ");
    while(firstName==="" || Number(firstName)){
        firstName = prompt("Enter your name again-", firstName);
   }
    let lastName = prompt("Enter your surname - ");
    while(lastName==="" || Number(lastName)){
        lastName = prompt("Enter your surname again-", lastName);
   }
   let getName;
   let getSurName;
   newUser={
    firstName: {
        value:firstName,
        writable:false,
    },
    lastName:{
        value:lastName,
        writable:false,
    },
    getLogin: function(){
        getName = this.firstName.value[0];
        getSurName = this.lastName.value;
        initials = getName+getSurName;
        alert(`Your initials - ${initials.toLowerCase()}`)
    }
   }
   return newUser.getLogin();
}
createNewUser();

//////WITH EXTRA TASK//////////
// const createNewUser=() => {
//     let firstName= prompt("Enter your name - ");
//     while(firstName==="" || Number(firstName)){
//         firstName = prompt("Enter your name again-", firstName);
//    }
//     let lastName = prompt("Enter your surname - ");
//     while(lastName==="" || Number(lastName)){
//         lastName = prompt("Enter your name again-", lastName);
//    }

 

//    const newUser= Object.create({}, {
// 	_firstName: {
//         value:firstName,
//         writable:false,
//     },
	
	
// 	_lastName:{
//     value:lastName,
//     writable:false,
//     },
	

// 	getLogin: function(){
// 		let _getName;
// 		let _getSurName;
// 		let _initials;
// 		    getName = this._firstName.value[0];
			
			
// 		    getSurName = this._lastName.value;
// 		    initials = getName+getSurName;
// 		    alert(`Your initials - ${initials.toLowerCase()}`)
// 		}
//    });

//     return newUser.getLogin();
//    }
// createNewUser();




