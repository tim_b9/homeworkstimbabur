
// Код для завдань лежить в папці project.


// Знайти всі параграфи на сторінці та встановити колір фону #ff0000


// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.


// Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
// This is a paragraph

// Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.


// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.


//1
// let pColored = document.querySelectorAll('p');
// pColored.forEach(element => {
//   element.style.background="#ff0000";
// });

//2
// let optElem=document.getElementById("optionsList")
// console.log(optElem);
// console.log(optElem.parentElement);
// console.log(optElem.childNodes);

// //3
//  тут елемент не с классом, а с id)
// let testPar =document.querySelector("#testParagraph");
// testPar.innerText="This is a paragraph!"
// console.log(testPar.innerText)

//4
let mainElem = document.querySelector('.main-header');
let mainItem=mainElem.childNodes; //returns mainItem - a collection of childNodes of mainElem
      //1 
// for(let item of mainItem){
//   if ( item.nodeName !== '#text' ){
//     item.classList.add( 'nav-item' )
//     console.log(item)
//  } 
// }
     // 2
mainItem.forEach(item =>{
  if ( item.nodeName !== '#text' ){
    item.classList.add( 'nav-item' )
    console.log(item)
}
});

// //5 
// let secTit=document.querySelectorAll('.section-title');
// secTit.forEach(item => {
//   item.classList.remove('section-title')
//   console.log(item)
// })

///