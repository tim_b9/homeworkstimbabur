// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//   кожен із елементів масиву вивести на сторінку у вигляді пункту списку;


//// DONE
let array=["Hello", "Tim", "Hey", "It", "Works"]
let tagName=prompt("In what tag do you need to write elements of array?")
while(tagName==="" || Number(tagName)){
  tagName = prompt("Enter your name again-", tagName);
}
const createUl=(tagName="body", array)=>{

  let createEl=document.createElement(`${tagName}`)
  createEl.style.cssText=`
  background-color:#dedede;
  margin:0 auto;
  font-size:18px;
  line-height:1.7;
  padding:20px;
  `;
  
  let createList= document.createElement("ul");
  createList.classList.add("list")
  createEl.prepend(createList);
  for(i=0; i<array.length; i++){
    let createLi=document.createElement("li");
    createLi.classList.add("list-item")
    createLi.innerText=`${i+1} Argument - ${array[i]}`;
    createList.append(createLi)
   
  }
   return document.body.prepend(createEl)
  
  }
  createUl(tagName, array)


  
// let tagName=prompt("In what tag do you need to write arguments?")
// const createUl=(tagName="body", ...argum)=>{

//   let createEl=document.createElement(`${tagName}`)
//   createEl.style.cssText=`
//   background-color:#dedede;
//   margin:0 auto;
//   font-size:18px;
//   line-height:1.7;
//   padding:20px;
//   `;
  
//   let createList= document.createElement("ul");
//   createList.classList.add("list")
//   createEl.prepend(createList);
//   for(i=0; i<argum.length; i++){
//     let createLi=document.createElement("li");
//     createLi.classList.add("list-item")
//     createLi.innerText=`${i+1} Argument - ${argum[i]}`;
//     createList.append(createLi)
   
//   }
//    return document.body.prepend(createEl)
  
//   }
//   createUl(tagName, "hello", "21y.o", "hey",4, true)
