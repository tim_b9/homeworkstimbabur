let offset=0
const sliderLine=document.querySelector(".slider-line")

let buttonNext=document.querySelector(".slider-next")
let buttonPrev=document.querySelector(".slider-prev")

buttonNext.addEventListener('click', ()=>{
    offset+=800
    if(offset>3200){
        offset=0
    }
    
    sliderLine.style.left=-offset+'px'
})

buttonPrev.addEventListener('click', ()=>{
    offset-=800
    if(offset<0){
        offset=3200
    }
    
    sliderLine.style.left=-offset+'px'
})

