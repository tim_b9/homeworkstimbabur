/*
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.
*/

// const typeText=(elem)=>{
//     elem.type= "text"
// }
// const typePassword=(elem)=>{
//     elem.type= "password"
// }

// let input1=document.querySelector("#input1")
// let input2=document.querySelector("#input2")

// let iClick1=document.querySelector(".iClick1")
// console.log(iClick1)

// iClick1.addEventListener('click', ()=>{
//     if(iClick1.classList.contains("fa-eye")){
//         iClick1.classList.replace("fa-eye", "fa-eye-slash")
//         typeText(input1)
        
//     }
//     else{
//         iClick1.classList.replace("fa-eye-slash", "fa-eye")
//         typePassword(input1)
//     }
// })


// let iClick2=document.querySelector(".iClick2")
// console.log(iClick2)

// iClick2.addEventListener('click', ()=>{
//     if(iClick2.classList.contains("fa-eye")){
//         iClick2.classList.replace("fa-eye", "fa-eye-slash")
//         typeText(input2)
//     }
//     else{
//         iClick2.classList.replace("fa-eye-slash", "fa-eye")
//         typePassword(input2)
//     }
// })


// let resultDiv=document.querySelector(".result")

// let btn = document.querySelector(".btn")
// btn.addEventListener('click', ()=>{
//     let inp1Value=input1.value
//     let inp2Value=input2.value
//     if(inp1Value===inp2Value){
//         alert("Данные совпадают")
//         resultDiv.classList.remove("errored")
//        resultDiv.style.display="inline-block"
//        resultDiv.classList.add("matched")
//        resultDiv.innerText="Данные отправлены"
//     }
//     else if(inp1Value!==inp2Value){
//         resultDiv.classList.remove("matched")
//        resultDiv.style.display="inline-block"
//        resultDiv.classList.add("errored")
//        resultDiv.innerText="Данные не верны"  
//     }
// })


const typeText=(elem)=>{
    elem.type= "text"
}
const typePassword=(elem)=>{
    elem.type= "password"
}

let input1=document.querySelector("#input1")
let input2=document.querySelector("#input2")

let iClick1=document.querySelector(".iClick1")
iClick1.addEventListener('click', ()=>{
    if(iClick1.classList.contains("fa-eye")){
        iClick1.classList.replace("fa-eye", "fa-eye-slash")
        typeText(input1)   
    }
    else{
        iClick1.classList.replace("fa-eye-slash", "fa-eye")
        typePassword(input1)
    }
})


let iClick2=document.querySelector(".iClick2")
iClick2.addEventListener('click', ()=>{
    if(iClick2.classList.contains("fa-eye")){
        iClick2.classList.replace("fa-eye", "fa-eye-slash")
        typeText(input2)
    }
    else{
        iClick2.classList.replace("fa-eye-slash", "fa-eye")
        typePassword(input2)
    }
})


let resultAlert=document.querySelector(".resultAlert")
let btn = document.querySelector(".btn")
btn.addEventListener('click', ()=>{
    let inp1Value=input1.value
    let inp2Value=input2.value
    if(inp1Value===inp2Value){
        alert("Данные совпадают")
        resultAlert.classList.remove("errored")
        resultAlert.style.display="inline-block"
        resultAlert.classList.add("matched")
        resultAlert.innerText="Данные отправлены"
    }
    else if(inp1Value!==inp2Value){
        resultAlert.classList.remove("matched")
        resultAlert.style.display="inline-block"
        resultAlert.classList.add("errored")
        resultAlert.innerText="Данные не верны"  
    }
})




