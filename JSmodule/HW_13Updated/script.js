/*
У папці banners лежить HTML код та папка з картинками.

При запуску програми на екрані має відображатись перша картинка.

Через 3 секунди замість неї має бути показано друга картинка.
Ще через 3 секунди – третя.
Ще через 3 секунди – четверта.

Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.

Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.

Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.

Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.

Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
*/

//wrapper
let images=document.querySelectorAll(".image-to-show")
let currentImage=1
let timerId;

// BUTTONS
let stopBtn= document.querySelector("#stopBtn")
let continueBtn= document.querySelector("#continueBtn")

const showImages=()=>{
    images[currentImage].style.display="none"
    if(currentImage===images.length-1){
        currentImage=0
    }
    else{
        currentImage++
    }
    images[currentImage].style.display="block"

     timerId=setTimeout(showImages, 3000)
}

// disable button while loop is working
continueBtn.setAttribute("disabled", "");


stopBtn.addEventListener("click",()=>{
  
    continueBtn.removeAttribute("disabled");
    clearTimeout(timerId)
    console.log(`Stoped`)

})

continueBtn.addEventListener("click",()=>{
    continueBtn.setAttribute("disabled", "");
    setTimeout(()=>{
        showImages()
        console.log(`Continued`)
    }, 3000)
    
})

showImages()

