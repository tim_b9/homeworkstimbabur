
document.querySelector(".themetoggle").addEventListener('click', (event)=>{
    event.preventDefault()
    if(localStorage.getItem('theme')==='dark'){
        localStorage.removeItem('theme');
        document.body.classList.remove("dark")
     }
     else{
        localStorage.setItem('theme','dark')
        document.body.classList.add("dark")
     } 
})

if(localStorage.getItem('theme')==='dark'){
    document.body.classList.add("dark")
}



/*

При завантаженні сторінки показати користувачеві поле вводу (input) з написом Price. Це поле буде служити для введення числових значень

Поведінка поля має бути наступною:
При фокусі на полі вводу - має з'явитися рамка зеленого кольору. У разі втрати фокусу вона пропадає.

Коли прибрано фокус з поля - його значення зчитується, над полем створюється span, у якому має бути текст: Поточна ціна: ${значення з поля вводу}. Поруч із ним має бути кнопка з хрестиком (X). Значення всередині поля вводу забарвлюється у зелений колір.
При натисканні на Х - span з текстом та кнопка X повинні бути видалені. Значення, введене у поле вводу, обнулюється.
Якщо користувач ввів число менше 0 – при втраті фокусу підсвічувати поле введення червоною рамкою, під полем виводити фразу – Please enter correct price. span з некорректним значенням при цьому не створюється.

*/

let removeItem = ( event ) => {
	if ( event.target.nodeName === "BUTTON" ) {
		event.target.parentElement.remove();
	}
}
document.body.addEventListener('click', (event)=>{
removeItem(event)
})

input.onblur = function() {
    if (!Number(input.value)) { // not number
    input.classList.remove("focused")
      input.classList.add('invalid');
      output.style.display="block"
      let error=document.createElement("p")
      error.innerHTML="Please enter correct price (Number)"
      output.append(error)
    }
    else{
        output.style.display="block"
        let properPrise=document.createElement("p")
        properPrise.innerHTML=`Current price - ${input.value} <button type="submit" id="removeBtn">X</button>`
        output.append(properPrise)
    }
  };
  
  input.onfocus = function() {
    if (input.classList.contains('invalid')) {
      input.classList.remove('invalid');
    }
    input.classList.add("focused")
  };
