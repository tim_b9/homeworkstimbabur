// fibonacci

// RECURSION
// function fibonacci(n){
//     if(n<=1){
//         return n
//     }
//     return fibonacci(n-1)+ fibonacci(n-2)
// }
// console.log(fibonacci(4))

// LOOP
function fibonacciLoop(a,b,n){
    let f1=a
    let f2=b
    current=1
    for(let i=1; i<=n; i++){
        current=f1+f2
        f1=f2
        f2=current
    }

    return current
}

console.log(fibonacciLoop(1,1,3)); // 1,1,2,3,5