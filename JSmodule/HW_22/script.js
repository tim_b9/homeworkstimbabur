
/*
Створити поле 30*30 з білих клітинок за допомогою елемента `````.

При натисканні на білу клітинку вона повинна змінювати колір на чорний. При натисканні на чорну клітинку вона повинна змінювати колір назад на білий.

Сама таблиця повинна бути не вставлена у вихідний HTML-код, а згенерована і додана в DOM сторінки за допомогою Javascript.

Обработчик события click нужно повесить на всю таблицу. События всплывают от элемента вверх по DOM дереву, их все можно ловить с помощью одного обработчика событий на таблице, и в нем определять, на какую из ячеек нажал пользователь.

При клике на любое место документа вне таблицы, все цвета клеточек должны поменяться на противоположные (подсказка: нужно поставить Event Listener на <body>).

Чтобы поменять цвета всех клеточек сразу, не нужно обходить их в цикле. Если помечать нажатые клетки определенным классом, то перекрасить их все одновременно можно одним действием - поменяв класс на самой таблице.
*/

let tableRows=20;
let tableColumns=20;

let board = document.createElement( 'section' );
board.classList.add( 'board' );
document.body.append( board );

// create table
function table() {
	for ( let i = 1; i <=tableRows * tableColumns; i++ ) {
		const cell = document.createElement( "div" )
		cell.classList.add( "cell" );
		board.appendChild( cell )
	}
}
table()



board.addEventListener('click', (event)=>{
  if(event.target.nodeName==="DIV"){
    event.target.classList.toggle("black-style")
  }
})

document.body.addEventListener("click", (event)=>{
  if(event.target.nodeName!=="SECTION" && event.target.nodeName!=="DIV"){
    let cells=document.querySelectorAll(".cell")
    cells.forEach(cell=>{
      cell.classList.toggle("black-style")
    })
    
  }
})


