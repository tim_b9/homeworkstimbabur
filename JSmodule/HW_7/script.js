// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.

// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

// "Первым параметром будет что-то в типе, который вы не хотите видеть в массиве, 
// дальше передаются сами агрументы массива");

const filterBy = (typeOf, ...argum) =>{
const array=[];

  argum.forEach(item =>{
    if(typeof item == typeof typeOf){
      return console.log(`${item} will not be in array, because its ${typeof item}`)
    }
    else{
      array.push(`${item} has type of ${typeof item}`);
    }
  })


return array
}
console.log(filterBy(21, 1 , 22, "Hello", true, "it works", 42, 432))

