/*
При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". 
Дана кнопка повинна бути єдиним контентом у тілі HTML документа, решта контенту потрібно створити і додати на сторінку за допомогою Javascript.
При натисканні на кнопку "Намалювати коло" показувати одне поле введення - діаметр кола. При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. При кліку на конкретне коло - це коло має зникати, у своїй порожнє місце заповнюватися, тобто інші кола зрушуються вліво.
У вас може виникнути бажання поставити обробник події на кожне коло для його зникнення. Це неефективно, так не треба робити. На всю сторінку має бути лише один обробник подій, який це робитиме.
*/


document.querySelector(".themetoggle").addEventListener('click', (event)=>{
    event.preventDefault()
    if(localStorage.getItem('theme')==='dark'){
        localStorage.removeItem('theme');
        document.body.classList.remove("dark")
     }
     else{
        localStorage.setItem('theme','dark')
        document.body.classList.add("dark")
     } 
})

if(localStorage.getItem('theme')==='dark'){
    document.body.classList.add("dark")
}

//task

function getRandomColor() {
	const r = Math.floor( Math.random() * 255 );
	const g = Math.floor( Math.random() * 255 );
	const b = Math.floor( Math.random() * 255 );
	return `rgb(${ r }, ${ g }, ${ b })`;
}

let counter=0
document.addEventListener("click", (event)=>{

        if(event.target.nodeName==="BUTTON"){
            counter++
            if(counter>2)return
            console.log(counter)

            let p=document.createElement("p")
            p.innerText="Loading..."
            document.body.append(p)
            console.log("button clicked")
            setTimeout(()=>{
            p.remove()
            for(let i=1; i<=100; i++){
            let createDiv=document.createElement("div")
            createDiv.style.cssText=`
            background-color:${getRandomColor()};
            display:inline-block;
            width:25px;
            height:25px;
            border-radius:50%;
            margin:1px
            `
            document.body.append(createDiv)
            }
            }, 1500)   
        }
        if(event.target.nodeName==="DIV"){
            event.target.remove()
        }
})
