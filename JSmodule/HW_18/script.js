
//1
// Через Рекурсию

const users={

	"bapor":{
		parent:{
			"guten":{
				parent:{
					"gayna":{
						parent:{
							"aleva":{

							}
						}
					},
					"gavana":{

					},
				}
			}
		},
	},
	"guray":{
		parent:{
			"hopkov":{
				parent:{
					"laron":{

					},
					"buray":{
						parent:{
							"huana":{

							}
						}
					},				
				},
			}
		},
	},
	 	
}

function userParentRecursion(obj){

		for(let key in obj){
			console.log(key)
			userParentRecursion(obj[key])
		}
	
}
userParentRecursion(users)


//2 
// через map
/*

const users=[
  {
    "name": "Tim  ", 
    age: {
      "hey": "im in",
      "hey2": {
        "hey3":"im in too"
      }
    }
  },
  {
    "name": " Alex  Booms ", age: 31
  },
];
const newUsers=users.map(item=>{
item.name = item.name.trim().toLocaleLowerCase()
return item
})
console.log(newUsers)

*/


//3
// Через Filter

/*
const users=[
  {
    "name": "Tim  ", 
    age: {
      "hey": "im in",
      "hey2": {
        "hey3":"im in too"
      }
    }
  },
  {
    "name": " Alex  Booms ", age: 31
  },
];
const newUsers=users.filter((item)=>{
  return item
})

console.log(newUsers)

*/

