// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.

const createNewUser = () => {
	let firstName;
	let lastName;
    let birthday;
	let usYear;
	do {
		firstName = prompt( "Enter your name - " );
	} while ( !Boolean( firstName ) || !isNaN( Number( firstName ) ) );

	do {
		lastName = prompt( "Enter your surname - " );
	} while ( !Boolean( lastName ) || !isNaN( Number( lastName ) ) );

    do {
		birthday = prompt( "Enter your birthday - " );
	} while ( !Boolean( birthday ) );
	const newUser = {};
	// Добавляем объекту newUser свойство 'firstName' и запрещаем его перезапись через точку
	Object.defineProperty( newUser, 'firstName', {
		enumerable: true,
		configurable: true,
		writable: false, // запрещаем перезапись через точку
		value: firstName
	} );

	Object.defineProperty( newUser, 'lastName', {
		enumerable: true,
		configurable: true,
		writable: false,
		value: lastName
	} );

	newUser.setFirstName = function ( val ) {
		Object.defineProperty( this, 'firstName', {
			value: val
		} );
		return true;
	}

	newUser.setLastName = function ( val ) {
		Object.defineProperty( this, 'lastName', {
			value: val
		} );
		return true;
	}

	newUser.getLogin = function () {
		return ( this.firstName[ 0 ].toUpperCase() + this.lastName.toLowerCase());
	}
    newUser.getAge = function () {
        let birthDate=new Date(birthday);
        birthDate.setMonth(birthDate.getMonth()+1)
        usYear=birthDate.getFullYear();
        let now=new Date()
        let nowYear=now.getFullYear();
        let userAge=nowYear-usYear;
		return userAge;
	}
    newUser.getPassword= function () {
		return this.getLogin() + birthday.slice(6);
	}

	return newUser;
}
alert(createNewUser().getPassword()); // initials
// alert(createNewUser().getAge()); // user age

